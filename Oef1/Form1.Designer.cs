﻿namespace Oef1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFillValue = new System.Windows.Forms.Label();
            this.txtFillValue = new System.Windows.Forms.TextBox();
            this.btnFill = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStartRow = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEndRow = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEndCol = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtStartCol = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblFillValue
            // 
            this.lblFillValue.AutoSize = true;
            this.lblFillValue.Location = new System.Drawing.Point(41, 53);
            this.lblFillValue.Name = "lblFillValue";
            this.lblFillValue.Size = new System.Drawing.Size(95, 13);
            this.lblFillValue.TabIndex = 1;
            this.lblFillValue.Text = "Fill with this value: ";
            this.lblFillValue.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtFillValue
            // 
            this.txtFillValue.Location = new System.Drawing.Point(142, 50);
            this.txtFillValue.Name = "txtFillValue";
            this.txtFillValue.Size = new System.Drawing.Size(60, 20);
            this.txtFillValue.TabIndex = 2;
            this.txtFillValue.TextChanged += new System.EventHandler(this.txtFillValue_TextChanged);
            // 
            // btnFill
            // 
            this.btnFill.Location = new System.Drawing.Point(38, 81);
            this.btnFill.Name = "btnFill";
            this.btnFill.Size = new System.Drawing.Size(98, 27);
            this.btnFill.TabIndex = 3;
            this.btnFill.Text = "Fill";
            this.btnFill.UseVisualStyleBackColor = true;
            this.btnFill.Click += new System.EventHandler(this.btnFill_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(314, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Fill the following fields:";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // txtStartRow
            // 
            this.txtStartRow.Location = new System.Drawing.Point(317, 81);
            this.txtStartRow.Name = "txtStartRow";
            this.txtStartRow.Size = new System.Drawing.Size(33, 20);
            this.txtStartRow.TabIndex = 6;
            this.txtStartRow.TextChanged += new System.EventHandler(this.txtStartRow_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(259, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "From row: ";
            // 
            // txtEndRow
            // 
            this.txtEndRow.Location = new System.Drawing.Point(317, 112);
            this.txtEndRow.Name = "txtEndRow";
            this.txtEndRow.Size = new System.Drawing.Size(33, 20);
            this.txtEndRow.TabIndex = 8;
            this.txtEndRow.TextChanged += new System.EventHandler(this.txtEndRow_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(259, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "To row:";
            // 
            // txtEndCol
            // 
            this.txtEndCol.Location = new System.Drawing.Point(432, 112);
            this.txtEndCol.Name = "txtEndCol";
            this.txtEndCol.Size = new System.Drawing.Size(33, 20);
            this.txtEndCol.TabIndex = 12;
            this.txtEndCol.TextChanged += new System.EventHandler(this.txtEndCol_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(374, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "To col:";
            // 
            // txtStartCol
            // 
            this.txtStartCol.Location = new System.Drawing.Point(432, 81);
            this.txtStartCol.Name = "txtStartCol";
            this.txtStartCol.Size = new System.Drawing.Size(33, 20);
            this.txtStartCol.TabIndex = 10;
            this.txtStartCol.TextChanged += new System.EventHandler(this.txtStartCol_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(374, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "From col: ";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(39, 171);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(426, 200);
            this.textBox1.TabIndex = 13;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(38, 124);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(98, 27);
            this.btnReset.TabIndex = 14;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 450);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtEndCol);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtStartCol);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtEndRow);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtStartRow);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFill);
            this.Controls.Add(this.txtFillValue);
            this.Controls.Add(this.lblFillValue);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblFillValue;
        private System.Windows.Forms.TextBox txtFillValue;
        private System.Windows.Forms.Button btnFill;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStartRow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEndRow;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEndCol;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtStartCol;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnReset;
    }
}

