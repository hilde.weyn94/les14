﻿using System;
using System.Windows.Forms;

namespace Oef1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int startRow;
        int endRow;
        int startCol;
        int endCol;
        int fillValue;

        int[,] arOef1 = new int[10, 10];

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void txtStartRow_TextChanged(object sender, EventArgs e)
        {
            startRow = GetAndCheckInput(txtStartRow);
        }

        private void txtEndRow_TextChanged(object sender, EventArgs e)
        {
            endRow = GetAndCheckInput(txtEndRow);
        }

        private void txtStartCol_TextChanged(object sender, EventArgs e)
        {
            startCol = GetAndCheckInput(txtStartCol);
        }

        private void txtEndCol_TextChanged(object sender, EventArgs e)
        {
            endCol = GetAndCheckInput(txtEndCol);
        }

        private int GetAndCheckInput(TextBox txtBox)
        {
            int value;
            if (int.TryParse(txtBox.Text, out value) && !string.IsNullOrWhiteSpace(txtBox.Text))
            {
                if (value < 0 || value > 9)
                {
                    MessageBox.Show("This should be between 0-9");
                    txtBox.Clear();

                }
            }
            else if (!int.TryParse(txtBox.Text, out value) && !string.IsNullOrWhiteSpace(txtBox.Text))
            {
                MessageBox.Show("This should be an integer", "Input error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBox.Clear();
            }
            return value;
        }

        private void txtFillValue_TextChanged(object sender, EventArgs e)
        {
            if (int.TryParse(txtFillValue.Text, out fillValue) && !string.IsNullOrWhiteSpace(txtFillValue.Text))
            {

            }
            else if (!int.TryParse(txtFillValue.Text, out fillValue) && !string.IsNullOrWhiteSpace(txtFillValue.Text))
            {
                MessageBox.Show("FillValue should be an integer between 0-9");
                txtFillValue.Clear();
            }

        }

        private void btnFill_Click(object sender, EventArgs e)
        {
            for (int row = startRow; row <= endRow; row++)
            {
                for (int col = startCol; col <= endCol; col++)
                {
                    arOef1[row, col] = fillValue;
                }

            }
            PrintToTextbox();
        
        }
        private void PrintToTextbox()
        {
            for (int rij = 0; rij <= arOef1.GetUpperBound(0); rij++)
            {
                for (int kolom = 0; kolom <= arOef1.GetUpperBound(1); kolom++)
                {
                    textBox1.Text += arOef1[rij, kolom].ToString().PadRight(5);
                }
                textBox1.Text += Environment.NewLine;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }


        private void btnReset_Click(object sender, EventArgs e)
        {
            txtEndCol.ResetText();
            txtEndRow.ResetText();
            txtStartCol.ResetText();
            txtStartRow.ResetText();
            txtFillValue.ResetText();
            textBox1.ResetText();
        }
    }
}
