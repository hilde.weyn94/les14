﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Oef5
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }
        int[,] arOef5 = new int[5, 4];
        string[] headers = new string[] { "", "E", "F", "OV", "A" };
        string[] rowNames = new string[] { "<1960", "<1970", "<1980", "<1990", "2000-" };
        int colwidth = 10;


        private void btnVerwerken_Click(object sender, EventArgs e)
        {
            ReadTxtAndFillArray("Les 14 - Enquete.txt");
            PrintHeadersToTextbox(headers, txtShowFile);
            PrintArrayToTextbox(arOef5, txtShowFile);
        }

        private void ReadTxtAndFillArray(string fileName)
        {
            string[] line;

            if (System.IO.File.Exists(fileName))
            {
                using (StreamReader sr = new StreamReader(fileName))
                {
                    int row = 0;

                    while (!sr.EndOfStream)
                    {
                        line = sr.ReadLine().Split(',');
                        int year = int.Parse(line[1]);
                        string transport = line[2].TrimStart('\"').TrimEnd('\"');
                        if (year < 1960)
                        {
                            row = 0;
                            countTransportPerYear(transport, row);
                        }
                        else if (year < 1970)
                        {
                            row = 1;
                            countTransportPerYear(transport, row);
                        }
                        else if (year < 1980)
                        {
                            row = 2;
                            countTransportPerYear(transport, row);
                        }
                        else if (year < 1990)
                        {
                            row = 3;
                            countTransportPerYear(transport, row);
                        }
                        else
                        {
                            row = 4;
                            countTransportPerYear(transport, row);
                        }
                    }

                }
            }
            else
            {
                Console.WriteLine("het bestand werd niet gevonden");
            }
        }

        private void countTransportPerYear(string transport, int row)
        {
            int col;
            if (transport == "E")
            {
                col = 0;
                arOef5[row, col] += 1;
            }
            else if (transport == "F")
            {
                col = 1;
                arOef5[row, col] += 1;
            }
            else if (transport == "OV")
            {
                col = 2;
                arOef5[row, col] += 1;
            }
            else if (transport == "A")
            {
                col = 3;
                arOef5[row, col] += 1;
            }
        }
        private void PrintHeadersToTextbox(string[] headers, TextBox txtbox)
        {
            for (int kolom = 0; kolom < headers.Length; kolom++)
            {
                txtbox.Text += $"{headers[kolom]}".PadLeft(colwidth);
            }
            txtbox.Text += Environment.NewLine;
        }
        private void PrintArrayToTextbox(int[,] arrayToPrint, TextBox txtbox)
        {
            for (int rij = 0; rij <= arrayToPrint.GetUpperBound(0); rij++)
            {
                txtbox.Text += $"{rowNames[rij]}".PadLeft(colwidth);
                for (int kolom = 0; kolom <= arrayToPrint.GetUpperBound(1); kolom++)
                {
                    txtbox.Text += $"{arrayToPrint[rij, kolom]}".PadLeft(colwidth);
                }
                txtbox.Text += Environment.NewLine;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtShowFile_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
