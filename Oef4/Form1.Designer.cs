﻿namespace Oef4
{
    partial class btnRun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtOriginal = new System.Windows.Forms.TextBox();
            this.lblOriginal = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtSwitch = new System.Windows.Forms.TextBox();
            this.lblSwitch = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtMove = new System.Windows.Forms.TextBox();
            this.lblMove = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtFrom = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtTo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtOriginal);
            this.panel1.Controls.Add(this.lblOriginal);
            this.panel1.Location = new System.Drawing.Point(9, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(227, 107);
            this.panel1.TabIndex = 0;
            // 
            // txtOriginal
            // 
            this.txtOriginal.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOriginal.Location = new System.Drawing.Point(8, 28);
            this.txtOriginal.Multiline = true;
            this.txtOriginal.Name = "txtOriginal";
            this.txtOriginal.ReadOnly = true;
            this.txtOriginal.Size = new System.Drawing.Size(206, 61);
            this.txtOriginal.TabIndex = 1;
            this.txtOriginal.TextChanged += new System.EventHandler(this.txtOriginal_TextChanged);
            // 
            // lblOriginal
            // 
            this.lblOriginal.AutoSize = true;
            this.lblOriginal.Location = new System.Drawing.Point(10, 15);
            this.lblOriginal.Name = "lblOriginal";
            this.lblOriginal.Size = new System.Drawing.Size(75, 13);
            this.lblOriginal.TabIndex = 0;
            this.lblOriginal.Text = "Huidige array: ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtSwitch);
            this.panel2.Controls.Add(this.lblSwitch);
            this.panel2.Location = new System.Drawing.Point(9, 122);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(227, 107);
            this.panel2.TabIndex = 2;
            // 
            // txtSwitch
            // 
            this.txtSwitch.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSwitch.Location = new System.Drawing.Point(8, 28);
            this.txtSwitch.Multiline = true;
            this.txtSwitch.Name = "txtSwitch";
            this.txtSwitch.ReadOnly = true;
            this.txtSwitch.Size = new System.Drawing.Size(206, 61);
            this.txtSwitch.TabIndex = 1;
            // 
            // lblSwitch
            // 
            this.lblSwitch.AutoSize = true;
            this.lblSwitch.Location = new System.Drawing.Point(10, 15);
            this.lblSwitch.Name = "lblSwitch";
            this.lblSwitch.Size = new System.Drawing.Size(35, 13);
            this.lblSwitch.TabIndex = 0;
            this.lblSwitch.Text = "label2";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtMove);
            this.panel3.Controls.Add(this.lblMove);
            this.panel3.Location = new System.Drawing.Point(9, 235);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(227, 107);
            this.panel3.TabIndex = 3;
            // 
            // txtMove
            // 
            this.txtMove.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMove.Location = new System.Drawing.Point(8, 28);
            this.txtMove.Multiline = true;
            this.txtMove.Name = "txtMove";
            this.txtMove.ReadOnly = true;
            this.txtMove.Size = new System.Drawing.Size(206, 61);
            this.txtMove.TabIndex = 1;
            this.txtMove.TextChanged += new System.EventHandler(this.txtMove_TextChanged);
            // 
            // lblMove
            // 
            this.lblMove.AutoSize = true;
            this.lblMove.Location = new System.Drawing.Point(10, 15);
            this.lblMove.Name = "lblMove";
            this.lblMove.Size = new System.Drawing.Size(35, 13);
            this.lblMove.TabIndex = 0;
            this.lblMove.Text = "label3";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txtFrom);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Location = new System.Drawing.Point(9, 348);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(227, 41);
            this.panel4.TabIndex = 4;
            // 
            // txtFrom
            // 
            this.txtFrom.Location = new System.Drawing.Point(114, 9);
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new System.Drawing.Size(74, 20);
            this.txtFrom.TabIndex = 5;
            this.txtFrom.TextChanged += new System.EventHandler(this.txtFrom_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Index oorsprong: ";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.txtTo);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Location = new System.Drawing.Point(9, 395);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(227, 41);
            this.panel5.TabIndex = 7;
            // 
            // txtTo
            // 
            this.txtTo.Location = new System.Drawing.Point(114, 9);
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new System.Drawing.Size(74, 20);
            this.txtTo.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Index bestemming: ";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 332);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 11;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(9, 442);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(214, 29);
            this.button1.TabIndex = 12;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // btnRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 499);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "btnRun";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtOriginal;
        private System.Windows.Forms.Label lblOriginal;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtSwitch;
        private System.Windows.Forms.Label lblSwitch;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtMove;
        private System.Windows.Forms.Label lblMove;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtFrom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
    }
}

