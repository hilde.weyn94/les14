﻿using System;
using System.Windows.Forms;

namespace Oef4
{
    public partial class btnRun : Form
    {
        Random mijnRandom = new Random();
        int[,] originalArOef3 = new int[3, 5];
        int fromIndex;
        int toIndex;

        public btnRun()
        {
            InitializeComponent();
            FillArray(originalArOef3);
            PrintArrayToTextbox(originalArOef3, txtOriginal);


        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtMove.Text))
            {
                // validate input
                fromIndex = GetAndCheckIntegerFromTextbox(txtFrom);
                toIndex = GetAndCheckIntegerFromTextbox(txtTo);

                SwitchFromToIndex(); // ok
                MoveFromToIndex();
            }
            else
            {
                txtOriginal.Clear();
                txtMove.Clear();
                txtSwitch.Clear();
                txtFrom.Clear();
                txtTo.Clear();
                // een van de bovenstaande arrays moet in bovenste?
                PrintArrayToTextbox(originalArOef3, txtOriginal);
            }

        }

        private void SwitchFromToIndex()
        {
            int[,] arSwitch = CopyArray(originalArOef3);
            for (int row = 0; row < arSwitch.GetLength(0); row++)
            {
                int temp = arSwitch[row, toIndex];
                arSwitch[row, toIndex] = arSwitch[row, fromIndex];
                arSwitch[row, fromIndex] = temp;
            }
            PrintArrayToTextbox(arSwitch, txtSwitch);
        }

        private void MoveFromToIndex()
        {
            int[,] arMove = CopyArray(originalArOef3);
            if (fromIndex == toIndex)
            {

            }
            else if (fromIndex > toIndex)
            {
                for (int row = 0; row < arMove.GetLength(0); row++)
                {
                    for (int i = 0; i < arMove.GetLength(1); i++)
                    {
                        if (i > fromIndex || i < toIndex) // werkt
                        {
                            arMove[row, i] = originalArOef3[row, i];
                        }
                        else if (i == toIndex)
                        {
                            arMove[row, toIndex] = originalArOef3[row, fromIndex];
                        }
                        else
                        {
                            arMove[row, i] = originalArOef3[row, i - 1];
                        }

                    }
                }
            }
            else
            {
                for (int row = 0; row < arMove.GetLength(0); row++)
                {
                    for (int i = 0; i < arMove.GetLength(1); i++)
                    {
                        if (i == 0) // ok
                        {
                            arMove[row, 0] = originalArOef3[row, originalArOef3.GetUpperBound(1)];
                        }
                        else if (i == toIndex) //  ok
                        {
                            arMove[row, toIndex] = originalArOef3[row, fromIndex];
                        }

                        else if (i > fromIndex && i < toIndex)
                        {
                            arMove[row, i] = originalArOef3[row, i];
                        }
                        else
                        {
                            arMove[row, i] = originalArOef3[row, i - 1];
                        }

                    }
                }
            }
            PrintArrayToTextbox(arMove, txtMove);
            originalArOef3 = CopyArray(arMove);
        }


        private void FillArray(int[,] arrayToFill)
        {
            for (int row = 0; row < arrayToFill.GetLength(0); row++)
            {
                for (int kol = 0; kol < arrayToFill.GetLength(1); kol++)
                {

                    arrayToFill[row, kol] = mijnRandom.Next(0, 101);
                }
            }


        }

        private void PrintArrayToTextbox(int[,] arrayToPrint, TextBox destinationTxtBox)
        {
            for (int rij = 0; rij <= arrayToPrint.GetUpperBound(0); rij++)
            {
                for (int kolom = 0; kolom <= arrayToPrint.GetUpperBound(1); kolom++)
                {
                    destinationTxtBox.Text += arrayToPrint[rij, kolom].ToString().PadRight(5);
                }
                destinationTxtBox.Text += Environment.NewLine;
            }
        }

        private int[,] CopyArray(int[,] arrayToCopy)
        {
            int[,] destArray = new int[arrayToCopy.GetLength(0), arrayToCopy.GetLength(1)];
            for (int row = 0; row < arrayToCopy.GetLength(0); row++)
            {
                for (int col = 0; col < arrayToCopy.GetLength(1); col++)
                {
                    destArray[row, col] = arrayToCopy[row, col];
                }
            }
            return destArray;
        }

        private int GetAndCheckIntegerFromTextbox(TextBox txtBox)
        {
            int value;
            if (!int.TryParse(txtBox.Text, out value))
            {
                MessageBox.Show("This should be an integer", "Input error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBox.Clear();
            }

            return value;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void txtOriginal_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMove_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtFrom_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
