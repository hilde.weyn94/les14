﻿using System;
using System.Windows.Forms;

namespace Oef2
{

    public partial class Form1 : Form
    {
        Random mijnRandom = new Random();
        int[,] arOef2 = new int[5, 3];
        // int[,] arOef2 = new int[,] { { 45, 98, 143 }, { 26, 9, 35 }, { 0, 48, 48 }, { 42, 101, 143 }, { 12, 64, 76 } }; // test om max 2x af te drukken
        int sum = 0;
        int[] min = new int[] { 100, 0, 0 };
        int[] max = new int[] { 0 };
        int[] maxRow = new int[] { 0 };
        int[] maxCol = new int[] { 0 };



        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            FillArray();
            PrintArrayToTextbox();
            CalculateResults();
            PrintResults();
        }

        private void FillArray()
        {
            for (int kol = 0; kol < arOef2.GetLength(1) - 1; kol++)
            {
                for (int row = 0; row < arOef2.GetLength(0); row++)
                {
                    arOef2[row, kol] = mijnRandom.Next(0, 101);
                    arOef2[row, 2] = arOef2[row, 0] + arOef2[row, 1];
                }
            }

        }

        private void PrintArrayToTextbox()
        {
            for (int rij = 0; rij <= arOef2.GetUpperBound(0); rij++)
            {
                for (int kolom = 0; kolom <= arOef2.GetUpperBound(1); kolom++)
                {
                    textBox1.Text += arOef2[rij, kolom].ToString().PadRight(10);
                }
                textBox1.Text += Environment.NewLine;
            }

        }

        private void CalculateResults()
        {
            for (int row = 0; row <= arOef2.GetUpperBound(0); row++)
            {
                for (int kol = 0; kol <= arOef2.GetUpperBound(1); kol++)
                {
                    sum += arOef2[row, 2];
                    if (arOef2[row, kol] > max[0])
                    {
                        max[0] = arOef2[row, kol];
                        maxRow[0] = row + 1;
                        maxCol[0] = kol + 1;
                    }
                    else if (arOef2[row, kol] == max[0]) // idem voor min
                    {
                        Array.Resize(ref max, max.Length + 1);
                        max[max.Length - 1] = arOef2[row, kol];
                        Array.Resize(ref maxRow, maxRow.Length + 1);
                        maxRow[maxRow.Length - 1] = row + 1;
                        Array.Resize(ref maxCol, maxCol.Length + 1);
                        maxCol[maxCol.Length - 1] = kol + 1;

                    }
                    if (arOef2[row, kol] < min[0])
                    {
                        min[0] = arOef2[row, kol];
                        min[1] = row + 1;
                        min[2] = kol + 1;
                    }



                }
                textBox1.Text += Environment.NewLine;
            }

        }
        private void PrintResults()
        {
            txtResults.Text += $"De som van alle getallen is {sum}.";
            txtResults.Text += Environment.NewLine + Environment.NewLine;

            txtResults.Text += $"Het kleine getal is {min[0]}.";
            txtResults.Text += $"Dit staat op rij {min[1]} in kolom {min[2]}.";
            txtResults.Text += Environment.NewLine;
            for (int i = 0; i < max.Length; i++) // idem voor min
            {
                txtResults.Text += $"Het grootste getal is {max[i]}.";
                txtResults.Text += $"Dit staat op rij {maxRow[i]} in kolom {maxCol[i]}.";
                txtResults.Text += Environment.NewLine;
            }



        }
        private void btnSluiten_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtResults_TextChanged(object sender, EventArgs e)
        {


        }
    }
}
