﻿namespace Les14
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] testArray = new int[10, 10];
            int startrij = 1;
            int eindtrij = 2;
            int startcol = 0;
            int eindcol = 9;
            for (int i = startrij; i <= eindtrij; i++)
            {
                for (int j = startcol; j <= eindcol; j++)
                {
                    testArray[i, j] = 5;
                }
            }

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    System.Console.Write(testArray[i, j]);
                }
                System.Console.WriteLine();
            }

        }

    }
}
